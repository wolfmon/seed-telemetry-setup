#!/bin/bash

####################################################################
#
#	This script will disable telemetry for SEED VMs.
#
#	TODO:
#
#	1. Detect SEED VM, we don' want to install this tool elsewhere.
#			
####################################################################

# Guard directory, only execute this script when telemetry is enabled.

if [ ! -d "/home/seed/telemetry" ]; then
	echo "[SCRIPT ERROR] Telemetry is not enabled for this environment, exiting..."
    exit 11
fi

#restore bash pre-exec commands
sudo cp /etc/backup.bash.bashrc /etc/bash.bashrc
sudo cp /etc/backup.profile /etc/profile

# Stop syslog engine
sudo service syslog-ng stop

sudo rm -rf /home/seed/telemetry

echo "Telemetry Disabled. To re-enable, please run the setup_telemetry script again."

exit 0
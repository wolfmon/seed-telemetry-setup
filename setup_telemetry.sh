#!/bin/bash

####################################################################
#
#	This script will setup telemetry for SEED VMs.
#
#	TODO:
#
#	1. Detect SEED VM, we don' want to install this tool elsewhere.
#	2. Dispaly disclaimer and inform user how to disable it.
#	3. Built-in Self-test(BIST) to make sure it's working correctly.
#	4. Make sure this script DOES NOT execute more than once. 
#
####################################################################

#Command to execute in VM:
#wget https://gitlab.com/wolfmon/seed-telemetry-setup/raw/master/setup_telemetry.sh && chmod +x setup_telemetry.sh && ./setup_telemetry.sh && rm setup_telemetry.sh


#First Time Setup
#Create directory and UUID for telemetry use
if [ ! -d "/home/seed/telemetry" ]; then
	mkdir -p /home/seed/telemetry
fi

if [ ! -f "/home/seed/telemetry/uuid" ]
then
    uuidgen > /home/seed/telemetry/uuid
fi

#Make UUID available to others via env. variable
if [ -f "/home/seed/telemetry/uuid" ]
then
    export TELEMETRY_UUID=$(cat /home/seed/telemetry/uuid)
fi

#Install packages
sudo apt-get update
#Install syslog-ng
sudo apt-get install -y syslog-ng

# Download new configuration files
cd /home/seed/telemetry/
wget https://gitlab.com/wolfmon/seed-telemetry-setup/raw/master/telemetry_client.conf
wget https://gitlab.com/wolfmon/seed-telemetry-setup/raw/master/bash.bashrc
wget https://gitlab.com/wolfmon/seed-telemetry-setup/raw/master/profile
sudo cp /home/seed/telemetry/telemetry_client.conf /etc/syslog-ng/conf.d/

# Backup Origional Configure in case user want to deactivate telemetry.

sudo cp /etc/bash.bashrc /etc/backup.bash.bashrc
sudo cp /etc/profile /etc/backup.profile

# Replace with logger string enabled bash profile.
sudo cp /home/seed/telemetry/bash.bashrc /etc/bash.bashrc
sudo cp /home/seed/telemetry/profile /etc/profile

#Make sure the current shell is set for logging
#http://blog.kxr.me/2012/01/logging-shell-commands-in-linux.html
export PROMPT_COMMAND='RETRN_VAL=$?;logger -p user.notice "$(cat /home/seed/telemetry/uuid) ==> $(whoami) [$$]: $(history 1 | sed "s/^[ ]*[0-9]\+[ ]*//" ) [ReturnVal: $RETRN_VAL]"'

#Below configuration will also log command output, but will kind of mess up terminal output.
#export PROMPT_COMMAND='RETRN_VAL=$?;logger -p user.notice "$(cat /home/seed/telemetry/uuid) ==> $(whoami) [$$]: $(history 1 | sed "s/^[ ]*[0-9]\+[ ]*//" ) [$RETRN_VAL]";exec 1>&1 1> >(logger -s) >&1'

# sudo bash -c 'export PROMPT_COMMAND='RETRN_VAL=$?;logger -p user.notice "$(whoami) [$$]: $(history 1 | sed "s/^[ ]*[0-9]\+[ ]*//" ) [$RETRN_VAL]"' >> /etc/profile'
# sudo bash -c 'export PROMPT_COMMAND='RETRN_VAL=$?;logger -p user.notice "$(whoami) [$$]: $(history 1 | sed "s/^[ ]*[0-9]\+[ ]*//" ) [$RETRN_VAL]"' >> /etc/bash.bashrc'
# echo "export PROMPT_COMMAND='RETRN_VAL=$?;logger -p user.notice "$(whoami) [$$]: $(history 1 | sed "s/^[ ]*[0-9]\+[ ]*//" ) [$RETRN_VAL]"'" | sudo tee -a /etc/profile
# echo "export PROMPT_COMMAND='RETRN_VAL=$?;logger -p user.notice "$(whoami) [$$]: $(history 1 | sed "s/^[ ]*[0-9]\+[ ]*//" ) [$RETRN_VAL]"'" | sudo tee -a /etc/bash.bashrc


sudo service syslog-ng restart
#Install auditd and plugin for remote logging
#sudo apt-get install -y auditd audispd-plugins
#auditctl -a exit,always -F arch=b32 -F euid=0 -S execve -k root-command

#Install rootsh, a shell wrapper for logging
# cd /home/seed/telemetry
# git clone https://gitlab.com/wolfmon/seed-telemetry-setup.git
# cd seed-telemetry-setup/rootsh-1.5.3/
# #Disable local logging to save some space on student hard drive
# ./configure --disable-logfile
# make install
